function solution($A) {
    
    $values = array();
    
    foreach($A as $element) {
        $values[$element] = true;    
    }
    
    
    return count($values);
}